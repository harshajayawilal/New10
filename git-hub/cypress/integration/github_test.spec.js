describe ('Git Hub Test', ()=> {

    var validUsername = "harsha";
    var validEmail = "harsha@gmail.com";
    var validPassword = "git123";
    

    describe ('Login Related Tests', ()=> {
        beforeEach(function(){
          console.log('Test Started.')
          cy.visit('https://github.com/login')
          cy.contains('Sign in to GitHub')
        })

        afterEach(function(){
          console.log('Test Ended.')
        })

        //Test -01
        it('User cannot log in to Git Hub using an incorrect username', ()=> {
          cy.get('input[id="login_field"]').clear()
            .type('harshajayawilal9999')
            .should('have.value', 'harshajayawilal9999')

          cy.get('input[id="password"]').clear()
            .type('github111')
            .should('have.value', 'github111')

          cy.get('input[name="commit"]').click()

          cy.contains('Incorrect username or password.')
        })

        //Test - 02
        it('User cannot log in to Git Hub using an incorrect password', ()=> {
          cy.get('input[id="login_field"]').clear()
            .type(validUsername)
            .should('have.value', validUsername)

          cy.get('input[id="password"]').clear()
            .type('incorrectpassword123')
            .should('have.value', 'incorrectpassword123')

          cy.get('input[name="commit"]').click()

          cy.contains('Incorrect username or password.')
        })

        //Test - 03
        it('User cannot log in to Git Hub using an incorrect email which is not registered with', ()=> {
          cy.get('input[id="login_field"]').clear()
            .type('invalidemail@invalid9999.com')
            .should('have.value', 'invalidemail@invalid9999.com')

          cy.get('input[id="password"]').clear()
            .type(validPassword)
            .should('have.value', validPassword)

          cy.get('input[name="commit"]').click()

          cy.contains('Incorrect username or password.')
        })

        //Test - 04
        it('User cannot log in to Git Hub using a correct username and empty password', ()=> {
          cy.get('input[id="login_field"]').clear()
            .type(validUsername)
            .should('have.value', validUsername)
          cy.get('input[id="password"]').clear()
          cy.get('input[name="commit"]').click()

          cy.contains('Incorrect username or password.')
        })

        //Test -05
        it('User successfully logs in to Git Hub using correct username and password', ()=> {
          cy.get('input[id="login_field"]').clear()
            .type(validUsername)
            .should('have.value', validUsername)
          cy.get('input[id="password"]').clear()
            .type(validPassword)
            .should('have.value', validPassword)
          cy.get('input[name="commit"]').click()

          cy.get('ul[id="user-links"]')
          cy.contains('New repository')
        })
        
        //Test -06
        it('User successfully logs in to Git Hub using correct email and password', ()=> {
          cy.get('input[id="login_field"]').clear()
            .type(validEmail)
            .should('have.value', validEmail)
          cy.get('input[id="password"]').clear()
            .type(validPassword)
            .should('have.value', validPassword)
          cy.get('input[name="commit"]').click()

          cy.get('ul[id="user-links"]')
          cy.contains('New repository')
        })

    })

    describe ('Repo Management Tests', ()=> {
        beforeEach(function(){
          console.log('Test Started.')
          cy.visit('https://github.com/login')

          cy.get('body').then(($body) => {
            if($body.find('#login_field').length){
                cy.get('input[id="login_field"]').clear()
                    .type(validUsername)
                cy.get('input[id="password"]').clear()
                    .type(validPassword)
                cy.get('input[name="commit"]').click()
                cy.get('ul[id="user-links"]')
            }
            else{console.log('Already logged.')}
          })
        })

        afterEach(function(){
          console.log('Test Ended.')
        })

        var repoName = 'TestRepo-' + Math.floor(Math.random() * 100) + 1; //Repo name

        //Test -07
        it('User successfully creates new repository', function() {
            cy.get('.Box-title > .btn').click();

            cy.contains('Create a new repository')
            cy.get('#repository_name')
              .type(repoName)
              .should('have.value', repoName)
            cy.get('#repository_description')
              .type('Creating '+ repoName)
              .should('have.value', 'Creating '+ repoName)
            cy.get('#repository_public_true')
              .check()
            cy.get('#repository_auto_init')
              .check()
            cy.get('.btn-primary').click()

            cy.url()
              .should('include',validUsername +'/' + repoName)
            cy.get('.repository-meta-content > .col-11')
              .should('contain','Creating '+ repoName)
        })

        //Test -08
        it('User cannot create new repository with the name of an existing repository', ()=> {
            cy.get('.Box-title > .btn').click();

            cy.contains('Create a new repository')
            cy.get('#repository_name')
              .type(repoName)
            cy.get('#repository_description')
              .type('Creating '+ repoName)
            cy.get('#repository_public_true')
              .check()
            cy.get('#repository_auto_init')
              .check()
            cy.get('.btn-primary').click()

            cy.get('#js-flash-container > .flash')
              .should('contain','Repository creation failed.')
            cy.get('.error')
              .should('have.text','Name already exists on this account')
        })

        //Test -09
        it('User successfully commits a README.md file to a repository', ()=> {
          var repoNameCommit = 'TestRepo-' + Math.floor(Math.random() * 100) + 1; //Repo name

          cy.get('.Box-title > .btn').click();

          cy.contains('Create a new repository')
          cy.get('#repository_name')
            .type(repoNameCommit)
            .should('have.value', repoNameCommit)
          cy.get('#repository_description')
            .type('Creating '+ repoNameCommit)
            .should('have.value', 'Creating '+ repoNameCommit)
          cy.get('#repository_public_true')
            .check()
          cy.get('#repository_auto_init')
            .uncheck()
          cy.get('.btn-primary').click()

          cy.url()
            .should('include',validUsername +'/' + repoNameCommit)

          cy.get('a').contains('README').click();
          cy.url()
            .should('include',validUsername +'/' + repoNameCommit + '/new/master?readme=1')

          cy.get('input[name="filename"]')
            .clear()
            .type('README.md')
            .should('have.value', 'README.md')          
          cy.get('#commit-summary-input')
            .clear()
            .type('Creating README.md')
            .should('have.value', 'Creating README.md')
          cy.get('#commit-description-textarea')
            .clear()
            .type('Creating README.md for ' + repoNameCommit)
            .should('have.value', 'Creating README.md for ' + repoNameCommit)

          cy.get('#submit-file').click()

          cy.url()
          .should('eq','https://github.com/' + validUsername +'/' + repoNameCommit + '/tree/master')
          cy.get('span[itemprop="about"]')
            .should('contain','Creating ' + repoNameCommit)
      })

        //Test -10
        it('User successfully deletes an existing repository', ()=> {
          cy.visit('https://github.com/' + validUsername + '/' + repoName)
          cy.get('span[itemprop="about"]')
          .should('contain','Creating '+ repoName)

          cy.get('a[href="/'+ validUsername +'/' + repoName +'/settings"]').contains('Settings').click()
          cy.get('summary[aria-haspopup="dialog"]').contains('Delete this repository').click()
          cy.get('div[class="Box-title"]').should('have.text', 'Are you absolutely sure?')

          cy.get('input[name="verify"]')
            .eq(1).type(repoName)
          cy.get('button[type="submit"]')
            .contains('I understand the consequences, delete this repository').click()

          cy.url()
            .should('eq','https://github.com/')
          cy.get('#js-flash-container').children().get('div').children().get('div')
            .should('contain','our repository "harshajayawilal/' + repoName + '" was successfully deleted.')
        })

    })

})
